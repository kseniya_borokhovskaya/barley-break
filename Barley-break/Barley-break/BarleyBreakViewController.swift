//
//  BarleyBreakViewController.swift
//  Barley-break
//
//  Created by Kseniya Borokhovskaya on 4/17/18.
//  Copyright © 2018 Kseniya Borokhovskaya. All rights reserved.
//

import UIKit

class BarleyBreakViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ReloadBarleyBreakDataDelegate {
    
    private var game: BarleyBreakProtocol = BarleyBreak(amountOfCards: 16)
    
    func reloadData() {
        numbersCollectionView.reloadData()
    }
    
    @IBAction func newGame(_ sender: Any) {
        newGame()
    }
    
    private func newGame() {
        game.newGame()
        movesCount = 0
    }
    
    private func checkIfWin() {
        if game.checkIfWin() {
            newGame()
        }
    }
   
    
    @IBOutlet weak var movesLabel: UILabel! {
        didSet {
            movesCount = 0
        }
    }
    
    private(set) var movesCount = 0 {
        didSet {
            updateMovesCountLabel()
        }
    }
    private func updateMovesCountLabel() {
        let attributedString = NSAttributedString(string: "Moves: \(movesCount)",
            attributes: nil)
        movesLabel.attributedText = attributedString
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateMovesCountLabel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private var font: UIFont {
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(30.0))
    }
    
    @IBOutlet weak var numbersCollectionView: UICollectionView!{
        didSet {
            numbersCollectionView.delegate = self
            numbersCollectionView.dataSource = self
            game.reloadDataDelegate = self
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return game.contentOfCards.count
    }
        
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NumberCell", for: indexPath)
        if let numberCell = cell as? BarleyBreakCollectionViewCell {
                let text = NSAttributedString(string: String(game.contentOfCards[indexPath.item]), attributes: [.font:font])
                numberCell.numberLabel.attributedText = text
            }
        return cell
    }
    
    @IBOutlet var mainView: UIView!{
        didSet {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(moveNumber(byHandlingGestureRecognizedBy:)))
            swipeDown.direction = [.down]
            
            let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(moveNumber(byHandlingGestureRecognizedBy:)))
            swipeUp.direction = [.up]
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(moveNumber(byHandlingGestureRecognizedBy:)))
            swipeLeft.direction = [.left]
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(moveNumber(byHandlingGestureRecognizedBy:)))
            swipeRight.direction = [.right]
            
            mainView.addGestureRecognizer(swipeDown)
            mainView.addGestureRecognizer(swipeUp)
            mainView.addGestureRecognizer(swipeLeft)
            mainView.addGestureRecognizer(swipeRight)
        }
    }
    
    @objc func moveNumber(byHandlingGestureRecognizedBy recognizer: UISwipeGestureRecognizer){
        movesCount += 1
        switch recognizer.direction {
        case .left :
            if game.moveTo(direction: "left") {
                numbersCollectionView.reloadItems(at: [IndexPath(item: game.indexOfEmptyCard, section: 0), IndexPath(item: game.indexOfEmptyCard - 1, section: 0)] )
                checkIfWin()
            }
        case .right :
            if game.moveTo(direction: "right") {
                numbersCollectionView.reloadItems(at: [IndexPath(item: game.indexOfEmptyCard, section: 0), IndexPath(item: game.indexOfEmptyCard + 1, section: 0)] )
                checkIfWin()
            }
        case .up :
            if game.moveTo(direction: "up") {
                numbersCollectionView.reloadItems(at: [IndexPath(item: game.indexOfEmptyCard, section: 0), IndexPath(item: game.indexOfEmptyCard - game.amountOfNumbersInRow, section: 0)] )
            }
        case .down :
            if game.moveTo(direction: "down") {
                numbersCollectionView.reloadItems(at: [IndexPath(item: game.indexOfEmptyCard, section: 0), IndexPath(item: game.indexOfEmptyCard + game.amountOfNumbersInRow, section: 0)] )
                checkIfWin()
            }
        default:
            break
        }
    }
    
    @IBOutlet weak var newGameButton: UIButton!
    //    {
    //        didSet {
    //            updateNewGameButton()
    //        }
    //    }
    //
    //    private func updateNewGameButton() {
    //        let attributedString = NSAttributedString(string: traitCollection.verticalSizeClass == .compact ?  "New\ngame" : " New game ", attributes: nil)
    //        newGameButton.setAttributedTitle(attributedString, for: .normal)
    //    }
    
    
}


