//
//  BarleyBreakReversed.swift
//  Barley-break
//
//  Created by Kseniya Borokhovskaya on 4/27/18.
//  Copyright © 2018 Kseniya Borokhovskaya. All rights reserved.
//

import Foundation

class BarleyBreakReversed : BarleyBreakProtocol {
    
    var contentOfCards: [String] = []
    
    var indexOfEmptyCard = 0
    
    var amountOfCards: Int
    
    var reloadDataDelegate: ReloadBarleyBreakDataDelegate?
    
    required init(amountOfCards: Int) {
        self.amountOfCards = amountOfCards
        self.newGame()
    }
    
    func newGame() {
        contentOfCards = (1..<amountOfCards).map{ String($0) }.shuffled()
        contentOfCards.append(" ")
        indexOfEmptyCard = amountOfCards - 1
        reloadDataDelegate?.reloadData()
    }
    
    func checkIfWin() -> Bool {
        var isWin = true
        for (index, number) in contentOfCards.enumerated(){
            if index + Int(number)! != amountOfCards - 1 && number != " " {
                isWin = false
                break
            }
        }
        return isWin
    }
}
