//
//  BarleyBreakProtocol.swift
//  Barley-break
//
//  Created by Kseniya Borokhovskaya on 4/27/18.
//  Copyright © 2018 Kseniya Borokhovskaya. All rights reserved.
//

import Foundation

protocol BarleyBreakProtocol {
    
    var contentOfCards : [String] { get set }
    
    var reloadDataDelegate : ReloadBarleyBreakDataDelegate? { get set }
    
    var indexOfEmptyCard: Int { get set }
    
    var amountOfCards: Int { get set }
    
    var amountOfNumbersInRow : Int { get }
    
    mutating func newGame()
    func checkIfWin() -> Bool
    mutating func moveTo(direction: String) -> Bool
    
    init(amountOfCards: Int)
    
}

extension BarleyBreakProtocol {
    
    var amountOfNumbersInRow : Int {
        return Int(Double(self.amountOfCards).squareRoot())
    }
    
    mutating func moveTo(direction: String) -> Bool {
        var isSucceed = false
        switch direction {
        case "left":
            if indexOfEmptyCard % amountOfNumbersInRow != amountOfNumbersInRow - 1 {
                self.contentOfCards.swapAt(indexOfEmptyCard, indexOfEmptyCard + 1)
                self.indexOfEmptyCard += 1
                isSucceed = true
            }
        case "right":
            if indexOfEmptyCard % amountOfNumbersInRow != 0 {
                self.contentOfCards.swapAt(indexOfEmptyCard, indexOfEmptyCard - 1)
                self.indexOfEmptyCard -= 1
                isSucceed = true
            }
        case "up":
            if indexOfEmptyCard <= contentOfCards.count - amountOfNumbersInRow {
                self.contentOfCards.swapAt(indexOfEmptyCard, indexOfEmptyCard + amountOfNumbersInRow)
                self.indexOfEmptyCard += amountOfNumbersInRow
                isSucceed = true
            }
        case "down":
            if indexOfEmptyCard >= amountOfNumbersInRow {
                self.contentOfCards.swapAt(indexOfEmptyCard, indexOfEmptyCard - amountOfNumbersInRow)
                self.indexOfEmptyCard -= amountOfNumbersInRow
                isSucceed = true
            }
        default:
            break
        }
        return isSucceed
    }
    

}
