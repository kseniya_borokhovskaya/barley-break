//
//  BarleyBreak.swift
//  Barley-break
//
//  Created by Kseniya Borokhovskaya on 4/23/18.
//  Copyright © 2018 Kseniya Borokhovskaya. All rights reserved.
//

import Foundation

class BarleyBreak : BarleyBreakProtocol {
    
    var contentOfCards: [String] = []

    var indexOfEmptyCard = 0
    
    var amountOfCards: Int
    
    var reloadDataDelegate: ReloadBarleyBreakDataDelegate?

    required init(amountOfCards: Int) {
        self.amountOfCards = amountOfCards
        self.newGame()
    }
    
    func newGame() {
        contentOfCards = (1..<amountOfCards).map{ String($0) }.shuffled()
        contentOfCards.append(" ")
        indexOfEmptyCard = amountOfCards - 1
        reloadDataDelegate?.reloadData()
    }
    
    func checkIfWin() -> Bool {
        var isWin = true
        for (index, number) in contentOfCards.enumerated(){
            if index + 1 != Int(number) && number != " " {
                isWin = false
                break
            }
        }
        return isWin
    }
}

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
