//
//  ReloadBarleyBreakDataDelegate.swift
//  Barley-break
//
//  Created by Kseniya Borokhovskaya on 4/27/18.
//  Copyright © 2018 Kseniya Borokhovskaya. All rights reserved.
//

import Foundation

protocol ReloadBarleyBreakDataDelegate {
    func reloadData()
}
